# COMPSYS302 Python Assignment 2014 - Test Scripts #
### Created by Fraser Killip, Michael McKnight, James Wiratmadja ###
----

To use these test scripts:

1. Copy `test.py` and `test_resources` into the directory with your scripts.

2. Run `test.py`