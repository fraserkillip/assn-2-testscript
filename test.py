"""Colours to print:

Reset: \033[0m
Black: \033[90m
Red: \033[91m
Green: \033[92m
Yellow: \033[93m
Blue: \033[94m
Magenta: \033[95m
Cyan: \033[96m
White: \033[97m

"""

import subprocess, os, sys,re
from test_resources import q9support

# Check if they want it in verbose mode or not
verb = "-v" in sys.argv

logFile = open("log.txt", "w")
didWriteToLog = False

# Define an enum type thing
# Use Response.reverse_mapping[ordinal] to get the string version of it
def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    reverse = dict((value, key) for key, value in enums.iteritems())
    enums['reverse_mapping'] = reverse
    return type('Enum', (), enums)

Response = enum("correct", "format", "wrong", "missing")

def err(message):
    print "\033[91mERROR: \033[0m" + message

def warn(message):
    print "\033[93mWarning: \033[0m" + message
    
def info(message):
    print "\033[96mInfo: \033[0m" + message
    
def verbose(message):
    if verb: print "\033[95mVerbose: \033[0m" + message
    
def writeToLog(message):
    logFile.write(message)
    logFile.write("\n")
    global didWriteToLog
    didWriteToLog = True
    
def steppedRange(start, end, step):
    while start <= end:
        yield start
        start += step

def q1():
    """Test Question One"""
    print "Testing question one..."
    if not os.path.isfile("q1.py"):
        info("q1.py not found, skipping test")
        return Response.missing
    with open("test_resources/q1words.csv") as f:
        content = f.readlines()
        linecount = 0
        passcount = 0
        badformat = False
        fail = False
        for line in content:
            linecount += 1
            vals = line.split(',')
            vals[1] = vals[1].replace("\n","").replace("\r","") #Remove the newline chars
            process = subprocess.Popen("python q1.py", shell = True, cwd = os.getcwd(), stdin=subprocess.PIPE, stdout=subprocess.PIPE)
            output =  process.communicate(vals[0])[0].replace("\n","")
            if output.endswith(vals[1]):
                passcount += 1
            elif vals[1] in output:
                badformat = True
            else:
                fail = True
                writeToLog("function input: '" + vals[0] + "'\r\n\texpected output: '" + vals[1] + "'\r\n\treceived output: '" + output + "'")
    if badformat:
        warn("Your output for question one appears to be correct, but does not match the format specified in the marking sheet.")
        return Response.format
    if fail:
        return Response.wrong
    print "\033[92mDone! Passed " + str(passcount) + " of " + str(linecount) + "\033[0m"
    return Response.correct

def q2():
    """Test Question Two"""
    print "Testing question two..."
    if not os.path.isfile("q2.py"):
        info("q2.py not found, skipping test")
        return Response.missing
    import q2
    with open("test_resources/q2.csv") as f:
        content = f.readlines()
        linecount = 0
        passcount = 0
        badformat = False
        fail = False
        for line in content:
            linecount += 1
            vals = line.split(',')
            try:
                vals[0] = float(vals[0])
            except:
                pass
            try:
                vals[1] = float(vals[1])
            except:
                pass
            vals[2] = vals[2].replace("\n","").replace("\r","") #Remove the newline chars
            output = q2.int_mult(vals[0], vals[1])
            if str(output) == vals[2]:
                passcount += 1
            elif vals[2] in str(output):
                badformat = True
            else:
                fail = True
                writeToLog("function input: '" + str(vals[0]) + ", "+ str(vals[1]) + "'\r\n\texpected output: '" + str(vals[2]) + "'\r\n\treceived output: '" + str(output) + "'")
    if badformat:
        warn("Your output for question two appears to be correct, but does not match the format specified in the marking sheet.")
        return Response.format
    if fail:
        err("Your output for question one is not correct")
        return Response.wrong
    print "\033[92mDone! Passed " + str(passcount) + " of " + str(linecount) + "\033[0m"
    return Response.correct
    
    
def q5():
    """Test Question Five"""
    print "Testing question five..."
    if not os.path.isfile("q5.py"):
        info("q5.py not found, skipping test")
        return Response.missing
    with open("test_resources/primes.txt") as f:
        # Read the primes from file
        allprimes = f.readlines()
        # Convert them to ints
        allprimes = [int(string.replace("\n","")) for string in allprimes]
        
        # Vars
        linecount = 0
        passcount = 0
        badformat = False
        fail = False
        
        # Loop over
        for limit in steppedRange(1,100000,2500):
            linecount += 1
            # Call the process
            process = subprocess.Popen("python q5.py", shell = True, cwd = os.getcwd(), stdin=subprocess.PIPE, stdout=subprocess.PIPE)
            
            # Send the limit to the script and get the output
            output = process.communicate(str(limit))[0].replace("\n", "")
            output = re.findall("\d+",output)
            output = [int(string) for string in output if string.isdigit()]
            validprimes = [prime for prime in allprimes if prime<limit]
            if output[len(output)-len(validprimes):len(output)] == validprimes:
                passcount += 1
            else:
                fail = True
                
            if len(output) != len(validprimes):
                badformat = True
                
    if badformat:
        warn("The full output of q5 may not be correct. All correct prime numbers were detected, but some number additional number were found before them, if you have a number in your `input` message ignore this")

    if fail:
        return Response.wrong
    print "\033[92mDone! Passed " + str(passcount) + " of " + str(linecount) + "\033[0m"
    return Response.correct
    
def q9():
    """Test Question Nine"""
    print "Testing question nine..."
    if not os.path.isfile("q9.py"):
        info("q9.py not found, skipping test")
        return Response.missing
        
    import q9
    import string
    import time
    import random

    count = 100

    
    verbose("Generating random test strings...")
    original = []
    for i in range(count):
        original.append([''.join(random.choice(string.ascii_lowercase) for _ in range(random.randint(5,15))) for _ in range(500)])
    verbose("Testing sort function...")
    
    user = []
    for arr in original:
        arr = arr[:]
        user.append(arr)
    start = time.time()
    for i in range(count):
        user[i] = q9.alphasort(user[i])
    usertime = (time.time() - start)/count
    
    bad = []
    for arr in original:
        arr = arr[:]
        bad.append(arr)
    start = time.time()
    for i in range(count):
        bad[i] = q9support.bad_sort(bad[i])
    badtime = (time.time() - start)/count

    system = []
    for arr in original:
        arr = arr[:]
        system.append(arr)
    start = time.time()
    for i in range(count):
        system[i].sort()
        
    systemtime = (time.time() - start)/count

    valid = True
    for i in range(len(user)-1):
        for j in range(len(user[i])-1):
            valid &= user[i][j] == system[i][j]
    if not valid: error("alphasort() did not correctly sort all the elements in the list")
    
    verbose("Done... Average times are as follows:")
    
    verbose("Your code:\t" + str(usertime*1000) + 'ms')
    verbose("Python .sort()\t" + str(systemtime*1000) + 'ms')
    
    syslonger = usertime/systemtime > 1
    bubblelonger = usertime/badtime > 1
    
    verbose("Your code is " + str(usertime/systemtime if syslonger else systemtime/usertime)+  " times " + str("longer" if syslonger else "quicker") + " than .sort()")
    verbose("Your code is " + str(usertime/badtime if bubblelonger else badtime/usertime)+  " times " + str("longer" if bubblelonger else "quicker") + " than Bubble Sort")
    
    if bubblelonger:
        warn("Your code seems to take longer than bubble sort... you will likely not get marks for speed")
    
    return Response.correct if valid else Response.wrong


results = []
     
results.append(("q1",q1()))
results.append(("q2",q2()))

results.append(("q5",q5()))


results.append(("q9",q9()))


logFile.close()

if didWriteToLog:
    info("There were likely some errors during the test, rather than spamming your screen they have been written to log.txt")
else:
    # If we didn't write anything, remove the file
    os.remove("log.txt")

print results