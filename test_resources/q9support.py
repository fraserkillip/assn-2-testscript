def bad_sort(myList):
    end = len(myList)-1
    while (end!=-1):
        swapped=-1
        for i in range(0,end):
            if myList[i]>myList[i+1]:
                temp=myList[i]
                myList[i]=myList[i+1]
                myList[i+1]=temp
                swapped=i
        end=swapped
    return myList